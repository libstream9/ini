#include <stream9/ini/error.hpp>

#include <sstream>

namespace stream9::ini {

namespace fs = std::filesystem;

static std::string
make_message(std::string_view const prefix, error_code const code)
{
    std::ostringstream oss;
    oss << prefix << ": " << to_string(code);

    return oss.str();
}

std::string
to_string(error_code const ec)
{
    switch (ec) {
        case error_code::invalid_char:
            return "invalid character";
        case error_code::new_line_is_expected:
            return "new line is expected";
        case error_code::key_value_separator_is_expected:
            return "key value separator is expected";
        case error_code::section_name_is_expected:
            return "section name is expected";
        case error_code::section_terminator_is_expected:
            return "section terminator is expected";
        case error_code::empty_key:
            return "empty key";
        case error_code::duplicate_key:
            return "duplicate key";
        case error_code::duplicate_section_name:
            return "duplicate section_name";
        case error_code::invalid_section_name:
            return "invalid section name";
        case error_code::unknown_escape_sequence:
            return "unknown escape sequence";
        case error_code::unclosed_quote:
            return "unclosed quote";
        case error_code::no_such_section:
            return "no such section";
        case error_code::no_such_key:
            return "no such key";
        case error_code::cant_move_section:
            return "can't move section";
    }

    return "unknown error"; // LCOV_EXCL_LINE
}

std::ostream&
operator<<(std::ostream& os, error_code const& code)
{
    return os << to_string(code);
}

// error

error::
error(error_code const code)
    : std::runtime_error { make_message("INI error", code) }
    , m_code { code }
{}

error::
error(error_code const code, char const* const what)
    : std::runtime_error { what }
    , m_code { code }
{}

// parse_error

static std::string
build_parse_error_message(error_code const code, std::string_view const line,
                      int const line_no, int const pos, fs::path const& path)
{
    std::ostringstream oss;
    std::string const padding (static_cast<size_t>(pos), ' ');

    oss << "INI parse error: " << to_string(code) << " on " << path << "\n"
        << line_no << " | " << line << "\n"
        << "  | " << padding << "^---";

    return oss.str();
}

parse_error::
parse_error(error_code const code, int const line_no, int const position,
            std::string_view const line, path_t path)
    : error { code, build_parse_error_message(code, line, line_no, position, path).c_str() }
    , m_path { std::move(path) }
    , m_line_no { line_no }
    , m_position { position }
{}

} // namespace stream9::ini
