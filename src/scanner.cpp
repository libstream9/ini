#include <stream9/ini/scanner.hpp>

#include <cassert>
#include <cctype>

namespace stream9::ini {

using iterator = std::string_view::const_iterator;

//TODO [[unlikely]]

class internal_error : public std::exception
{
public:
    internal_error() = default;
};

class line_error : public internal_error
{
};

class invalid_char : public internal_error
{
public:
    invalid_char(std::string_view range)
        : m_range { range } {}

    // accessor
    auto range() const { return m_range; }

private:
    std::string_view m_range;
};

static bool
is_space(char const c)
{
    return c == ' ' || c == '\t';
}

static std::string_view
rtrim(std::string_view const s)
{
    if (s.empty()) return s;

    auto it = s.end() - 1;

    for (; it >= s.begin(); --it) {
        if (!is_space(*it)) break;
    }

    return { s.begin(), it + 1 };
}

static bool
is_new_line(char const c)
{
    return (c == '\n' || c == '\r');
}

static bool
is_new_line(iterator const it, iterator const end)
{
    if (it == end) return false;

    return *it == '\n' || *it == '\r';
}

static bool
is_comment_mark(char const c)
{
    return c == '#' || c == ';';
}

static bool
is_comment_mark(iterator const it, iterator const end)
{
    if (it == end) return false;

    return *it == '#' || *it == ';';
}

static bool
is_quote_mark(char const c)
{
    return c == '\'' || c == '"';
}

// space := +(' ' | '\t')
static bool
space(iterator& it, iterator const end, event_handler&)
{
    if (it == end) return false;

    if (!is_space(*it)) return false;

    while (it != end && is_space(*it)) {
        ++it;
    }

    return true;
}

// new_line := '\n' | '\r' | "\r\n"
static bool
new_line(iterator& it, iterator const end, event_handler&)
{
    if (it == end) return false;

    if (*it == '\n') {
        ++it;
        return true;
    }
    else if (*it == '\r') {
        ++it;
        if (it != end && *it == '\n') {
            ++it;
        }
        return true;
    }

    return false;
}

static bool
new_line_notify(iterator& it, iterator const end, event_handler& h)
{
    auto const begin = it;

    if (new_line(it, end, h)) {
        h.on_newline({ begin, it });
        return true;
    }

    return false;
}

static void
skip_to_eol(iterator& it, iterator const end, event_handler& h)
{
    for (; it != end; ++it) {
        if (new_line_notify(it, end, h)) break;
    }
}

// continuation := '\' new_line
static bool
continuation(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    auto const begin = it;

    if (*it == '\\' && new_line(++it, end, h)) {
        return true;
    }

    it = begin;
    return false;
}

// char := <any character> - ctrl_char
// ctrl_char := <0x00-0x1F, 0x7F> - '\t'
static bool
char_(iterator& it, iterator const end, event_handler&)
{
    if (it == end) return false;

    if (*it == '\t') {
        ++it;
        return true;
    }

    if (std::iscntrl(*it)) return false;

    ++it;
    return true;
}

static bool
utf16_seq(iterator& it, iterator const end, event_handler& h)
{
    auto const begin = it;

    if (it == end) {
        h.on_error(error_code::unknown_escape_sequence, { begin - 2, it });
        throw line_error();
    }

    if (std::distance(it, end) < 4) {
        h.on_error(error_code::unknown_escape_sequence, { begin - 2, end });
        throw line_error();
    }

    for (auto e = it + 4; it < e; ++it) {
        if (!std::isxdigit(*it)) {
            h.on_error(error_code::unknown_escape_sequence, { begin - 2, it + 1 });
            throw line_error();
        }
    }

    return true;
}

// blank_line := new_line
static bool
blank_line(iterator& it, iterator const end, event_handler& h)
{
    return new_line_notify(it, end, h);
}

// escape_seq := "\\" | "\'" | '\"' | "\0" | "\a" | "\b" | "\t" | "\r"
//               | "\n" | "\;" | "\#" | "\=" | "\[" | "\]" | "\x<4 hexdigit UTF-16 seq>
static bool
escape_seq(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (*it != '\\') return false;

    auto const begin = it;

    ++it;
    if (it == end) {
        h.on_error(error_code::unknown_escape_sequence, { begin, it });
        throw line_error();
    }

    switch (*it) {
        case '\\': case '\'': case '"': case '0': case 'a': case 'b': case 't':
        case 'r': case 'n': case ';': case '#': case '=': case '[': case ']':
            ++it;
            return true;
        case 'x':
            return utf16_seq(++it, end, h);
        default:
            break;
    }

    h.on_error(error_code::unknown_escape_sequence, { begin, it });
    throw line_error();
}

// quoted_name_char := escape_seq | (char - new_line)
static bool
quoted_name_char(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (escape_seq(it, end, h)) return true;

    if (is_new_line(it, end)) return false;

    if (!char_(it, end, h)) {
        throw invalid_char({ it, it + 1 });
    }

    return true;
}

// quoted_name := quote_mark +(quoted_name_char - quote_mark) quote_mark
static bool
quoted_name(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    auto const begin = it;

    if (!is_quote_mark(*it)) return false;

    auto const mark = *it;
    ++it;

    if (it == end) {
        h.on_error(error_code::unclosed_quote, { begin, it });
        throw line_error();
    }

    if (*it == mark || !quoted_name_char(it, end, h)) {
        it = begin;
        return false;
    }

    while (it != end) {
        if (*it == mark) {
            ++it;
            return true;
        }

        if (!quoted_name_char(it, end, h)) break;
    }

    h.on_error(error_code::unclosed_quote, { begin, it });
    throw line_error();
}

// comment_mark := '#' | ';'
static bool
comment_mark(iterator& it, iterator const end, event_handler&)
{
    if (it == end) return false;

    if (is_comment_mark(*it)) {
        ++it;

        return true;
    }

    return false;
}

// comment := comment_mark *(continuation | char)
static bool
comment(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    auto const begin = it;

    if (!comment_mark(it, end, h)) return false;

    while (it != end) {
        if (is_new_line(*it)) break;

        if (!(continuation(it, end, h) || char_(it, end, h))) {
            h.on_error(error_code::invalid_char, { it, it + 1 });
            throw line_error();
        }
    }

    h.on_comment({ begin, it });
    return true;
}

// comment_line := *space comment new_line
static bool
comment_line(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    space(it, end, h);

    if (!comment(it, end, h)) return false;

    if (!new_line_notify(it, end, h) && it != end) {
        // LCOV_EXCL_START unreachable
        h.on_error(error_code::new_line_is_expected, { it, it + 1 });
        throw line_error();
        // LCOV_EXCL_STOP
    }

    return true;
}

// unquoted_value_char := continuation | escape_seq | (char - (comment_mark | new_line))
static bool
unquoted_value_char(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (continuation(it, end, h)) return true;

    if (escape_seq(it, end, h)) return true;

    if (is_new_line(it, end)) return false;

    if (is_comment_mark(it, end)) return false;

    if (!char_(it, end, h)) {
        h.on_error(error_code::invalid_char, { it, it + 1 });
        throw line_error();
    }

    return true;
}

// unquoted_value := (unquoted_value_char - quote_mark) *unquoted_value_char
static bool
unquoted_value(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return true;

    if (is_quote_mark(*it)) return false;

    while (it != end) {
        if (!unquoted_value_char(it, end, h)) break;
    }

    return true;
}

// quoted_value_char := continuation | escape_seq | (char - new_line)
static bool
quoted_value_char(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (continuation(it, end, h)) return true;

    if (escape_seq(it, end, h)) return true;

    if (is_new_line(it, end)) return false;

    if (!char_(it, end, h)) {
        h.on_error(error_code::invalid_char, { it, it + 1 });
        throw line_error();
    }

    return true;
}

// quoted_value := quote_mark +(quoted_value_char - quote_mark) quote_mark
static bool
quoted_value(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (!is_quote_mark(*it)) return false;
    auto const mark = *it;

    auto const begin = it;

    ++it;
    if (it == end) {
        h.on_error(error_code::unclosed_quote, { begin, it });
        throw line_error();
    }

    while (it != end) {
        if (*it == mark) {
            ++it;
            return true;
        }

        if (!quoted_value_char(it, end, h)) break;
    }

    h.on_error(error_code::unclosed_quote,{ begin, it });
    throw line_error();
}

// value := quoted_value | unquoted_value
static bool
value(iterator& it, iterator const end, event_handler& h)
{
    if (quoted_value(it, end, h)) return true;

    if (unquoted_value(it, end, h)) return true;

    return false; // LCOV_EXCL_LINE unreachable
}

// unquoted_key_char := escape_seq | (char - ('=' | comment_mark | new_line))
static bool
unquoted_key_char(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (escape_seq(it, end, h)) return true;

    if (*it == '=') return false;

    if (is_new_line(*it)) return false;

    if (is_comment_mark(*it)) return false;

    if (!char_(it, end, h)) {
        h.on_error(error_code::invalid_char, { it, it + 1 });
        throw line_error();
    }

    return true;
}

// unquoted_key := (unquoted_key_char - quote_mark) *(key_char)
static bool
unquoted_key(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (is_quote_mark(*it)) return false;

    if (!unquoted_key_char(it, end, h)) return false;

    while (it != end) {
        if (!unquoted_key_char(it, end, h)) break;
    }

    return true;
}

// key := quoted_name | unquoted_key
static bool
key(iterator& it, iterator const end, event_handler& h)
{
    try {
        if (quoted_name(it, end, h)) return true;
    }
    catch (invalid_char const& e) {
        h.on_error(error_code::invalid_char, e.range() );
        throw line_error();
    }

    if (unquoted_key(it, end, h)) return true;

    return false;
}

// entry := *space key *space '=' *space value *space ?comment new_line
static bool
entry(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    space(it, end, h);

    auto const key_begin = it;

    key(it, end, h);

    auto const key = rtrim({ key_begin, it });
    if (key.empty()) {
        h.on_error(error_code::empty_key, { it, it });
        throw line_error();
    }

    space(it, end, h);

    if (it == end || *it != '=') {
        using err = error_code;

        auto const e = it == end ? end : it + 1;
        h.on_error(err::key_value_separator_is_expected, { it, e });

        throw line_error();
    }
    ++it;

    space(it, end, h);

    auto const value_begin = it;

    value(it, end, h);

    auto const value = rtrim({ value_begin, it });

    h.on_entry(key, value);

    //space(it, end, h);

    comment(it, end, h);

    if (!new_line_notify(it, end, h) && it != end) {
        // LCOV_EXCL_START unreachable
        h.on_error(error_code::new_line_is_expected, { it, it + 1 });
        throw line_error();
        // LCOV_EXCL_STOP
    }

    return true;
}

// unquoted_section_char := escape_seq | (char - (']' | new_line | comment_mark))
static bool
unquoted_section_char(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (escape_seq(it, end, h)) return true;

    if (*it == ']') return false;

    if (is_new_line(it, end)) return false;

    if (is_comment_mark(*it)) {
        h.on_error(error_code::invalid_section_name, { it, it + 1 });
        throw line_error();
    }

    if (!char_(it, end, h)) {
        h.on_error(error_code::invalid_section_name, { it, it + 1 });
        throw line_error();
    }

    return true;
}


// unquoted_section_name := (unquoted_section_char - quote_mark) *(unquoted_section_char)
static bool
unquoted_section_name(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    if (is_quote_mark(*it)) return false;

    if (!unquoted_section_char(it, end, h)) return false;

    while (it != end) {
        if (!unquoted_section_char(it, end, h)) break;
    }

    return true;
}

// section_name := quoted_name | unquoted_section_name
static bool
section_name(iterator& it, iterator const end, event_handler& h)
{
    try {
        if (quoted_name(it, end, h)) return true;
    }
    catch (invalid_char const& e) {
        h.on_error(error_code::invalid_section_name, e.range() );
        throw line_error();
    }

    if (unquoted_section_name(it, end, h)) return true;

    return false;
}

// section := *space '[' *space section_name *space ']' *space ?comment new_line
static bool
section(iterator& it, iterator const end, event_handler& h)
{
    if (it == end) return false;

    space(it, end, h);

    if (*it != '[') return false;
    ++it;

    space(it, end, h);

    auto const name_begin = it;

    if (!section_name(it, end, h)) {
        h.on_error(error_code::section_name_is_expected, { name_begin, it });

        throw line_error();
    }

    auto const name = rtrim({ name_begin, it });

    //space(it, end, h);

    if (*it != ']') {
        h.on_error(error_code::section_terminator_is_expected, { it, it + 1 });

        throw line_error();
    }
    ++it;

    h.on_section(name);

    space(it, end, h);

    comment(it, end, h);

    if (!new_line_notify(it, end, h) && it != end) {
        h.on_error(error_code::new_line_is_expected, { it, it + 1 });

        throw line_error();
    }

    return true;
}

// ini_file := *(comment_line | section | entry | blank_line)
static bool
ini_file(iterator& it, iterator const end, event_handler& h)
{
    while (it != end) {
        try {
            if (blank_line(it, end, h)) continue;
            if (comment_line(it, end, h)) continue;
            if (section(it, end, h)) continue;
            if (entry(it, end, h)) continue;
        }
        catch (line_error const&) {
            skip_to_eol(it, end, h);
        }
    }

    return true;
}

void
scan(std::string_view const content, event_handler& handler)
{
    auto it = content.begin();
    ini_file(it, content.end(), handler);
}

} // namespace stream9::ini
