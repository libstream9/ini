#include <stream9/ini/editor/line_index.hpp>

#include <algorithm>
#include <cassert>

namespace stream9::ini::editors {

line_index::
line_index(std::string_view const text)
{
    m_lines.push_back(0); // always have actual line + 1 end sentinel item

    for (size_t i = 0; i < text.size(); ++i) {
        if (text[i] == '\n') { //TODO CR, CRLF
            m_lines.push_back(i + 1);
        }
    }

    if (!text.empty() && text.back() != '\n') {
        m_lines.push_back(text.size());
    }
}

count_t line_index::
count() const
{
    return m_lines.size();
}

line_no_t line_index::
line_no(location_t const loc) const
{
    auto const it = std::upper_bound(m_lines.begin(), m_lines.end(), loc);
    auto const no = std::distance(m_lines.begin(), it - 1);

    return no;
}

location_t line_index::
beginning_of_line(line_no_t const no) const
{
    assert(no < count());

    return m_lines[no];
}

void line_index::
insert(line_no_t const no, location_t const loc, length_t const len)
{
    assert(no < count());

    auto const it = m_lines.begin() + no;
    assert(*it >= loc);

    m_lines.insert(it, loc);

    shift(no + 1, len);
}

void line_index::
remove(line_no_t const from, line_no_t const to)
{
    assert(from <= count() - 1); // cant remove sentinel
    assert(to <= count() - 1); // ditto
    assert(from <= to);

    if (from == to) return;

    auto const loc1 = beginning_of_line(from);
    auto const loc2 = beginning_of_line(to);
    auto const length = loc2 - loc1;

    auto begin = m_lines.begin() + from;
    auto end = m_lines.begin() + to;

    m_lines.erase(begin, end);

    shift(from, -length);
}

void line_index::
shift(line_no_t const no, offset_t const offset)
{
    assert(no < count());

    for (auto i = no; i < m_lines.size(); ++i) {
        m_lines[i] += offset;
    }
}

void line_index::
clear()
{
    m_lines.clear();
}

} // namespace stream9::ini::editors
