#include <stream9/ini/editor/region.hpp>

#include <cassert>

namespace stream9::ini::editors {

region::
region(location_t const loc, length_t const len)
    : m_location { loc }
    , m_length { len }
{
    assert(m_location >= -1);
    assert(m_length >= 0);
}

} // namespace stream9::ini::editors
