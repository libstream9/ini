#include <stream9/ini/editor/element.hpp>

#include <stream9/ini/editor/buffer.hpp>

#include <stream9/ini/escape.hpp>

#include <ostream>

namespace stream9::ini::editors {

static std::string
unescape_name(auto const& buffer, auto const& region)
{
    auto const s1 = buffer.text(region); //TODO too many copy!
    auto const s2 = unescape(s1);
    return std::string(unquote(s2));
}

static std::string
unescape_value(auto const& buffer, auto const& region)
{
    auto const s1 = buffer.text(region);
    auto const s2 = unescape_continuation(s1); //TODO efficiently
    auto const s3 = unescape(s2);
    return std::string(unquote(s3));
}

// element

element::
element(class buffer& buf, class region const& r, std::string value)
    : m_buffer { &buf }
    , m_mark { m_buffer->add_mark(r.location()) }
    , m_length { r.length() }
    , m_value { std::move(value) }
{}

void element::
assign(std::string_view const v)
{
    m_buffer->replace_text(*m_mark, m_length, escape(v));
    m_value = v;
}

void element::
shift_mark(offset_t const o)
{
    m_mark->shift(o);
}

std::ostream&
operator<<(std::ostream& os, element const& e)
{
    return os << e.text();
}

// section_name

section_name::
section_name(class buffer& buf, class region const& m)
    : element { buf, m, unescape_name(buf, m) }
{}

std::string section_name::
escape(std::string_view const s) const
{
    return escape_section(s);
}

// key

key::
key(class buffer& buf, class region const& m)
    : element { buf, m, unescape_name(buf, m) }
{}

std::string key::
escape(std::string_view const s) const
{
    return escape_key(s);
}

// value

value::
value(class buffer& buf, class region const& m)
    : element { buf, m, unescape_value(buf, m) }
{}

std::string value::
escape(std::string_view const s) const
{
    return escape_value(s);
}

} // namespace stream9::ini::editors
