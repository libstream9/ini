#include <stream9/ini/editor/mark_set.hpp>

namespace stream9::ini::editors {

bool mark_set::
contains(mark const& m) const
{
    auto const [lower, upper] = m_marks.equal_range(m);

    for (auto it = lower; it != upper; ++it) {
        if (it->get() == &m) return true;
    }

    return false;
}

unique_mark_ptr mark_set::
add(location_t const l)
{
    auto const it = m_marks.insert(std::make_unique<mark>(*this, l));
    return unique_mark_ptr { it->get() };
}

bool mark_set::
remove(mark const& m)
{
    auto const& [lower, upper] = m_marks.equal_range(m);
    for (auto it = lower; it != upper; ++it) {
        if (it->get() == &m) {
            m_marks.erase(it);
            return true;
        }
    }

    return false;
}

void mark_set::
remove(location_t const from, location_t const to)
{
    assert(from >= 0);
    assert(to >= 0);
    assert(from <= to);

    auto lower = m_marks.lower_bound(from);
    auto upper = m_marks.lower_bound(to);
    assert(lower <= upper);

    m_marks.erase(lower, upper);
}

void mark_set::
shift(location_t const l, offset_t const offset)
{
    auto it = m_marks.lower_bound(l);

    if (offset >= 0) {
        for (; it != m_marks.end(); ++it) {
            auto& m = *it;

            auto const loc = m->location() + offset;
            m->set_location(loc);
        }
    }
    else {
        for (; it != m_marks.end(); ++it) {
            auto m = std::move(*it);
            it = m_marks.erase(it);

            auto const loc = m->location() + offset;
            m->set_location(loc);

            m_marks.insert(std::move(m));
        }
    }
}

void mark_set::
move_to(mark const& m, location_t const l)
{
    auto const& [lower, upper] = m_marks.equal_range(m);
    assert(lower != m_marks.end());

    auto it = lower;
    for (; it != upper; ++it) {
        if (it->get() == &m) break;
    }
    assert(it != upper);

    auto mptr = std::move(*it);
    m_marks.erase(it);

    mptr->set_location(l);

    m_marks.insert(std::move(mptr));
}

void mark_set::
clear()
{
    m_marks.clear();
}

} // namespace stream9::ini::editors
