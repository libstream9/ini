#include <stream9/ini/editor/buffer.hpp>

#include <stream9/ini/editor/element.hpp>
#include <stream9/ini/editor/region.hpp>

#include <stream9/ini/escape.hpp>

namespace stream9::ini::editors {

static std::string& //TODO can't we do without this
adjust_end_of_text(std::string& text)
{
    if (!text.empty() && text.back() != '\n') {
        text.push_back('\n');
    }

    return text;
}

// buffer

buffer::
buffer()
    : buffer { "" }
{}

buffer::
buffer(std::string text)
    : m_text { std::move(adjust_end_of_text(text)) }
    , m_lines { m_text }
{}

std::string_view buffer::
text(region const& r) const
{
    auto const loc = r.location();
    auto const len = r.length();

    assert(loc + len <= m_text.size());

    return std::string_view(m_text).substr(loc, len) ;
}

count_t buffer::
line_count() const
{
    return m_lines.count() - 1; // discount sentinel
}

line_no_t buffer::
line_no(location_t const loc) const
{
    assert(loc <= m_text.size());

    return m_lines.line_no(loc);
}

line_no_t buffer::
line_no(mark const& m) const
{
    assert(contains_mark(m));

    return m_lines.line_no(m.location());
}

location_t buffer::
beginning_of_line(line_no_t const line_no) const
{
    return m_lines.beginning_of_line(line_no);
}

bool buffer::
contains_mark(mark const& m) const
{
    return m_marks.contains(m);
}

unique_mark_ptr buffer::
add_mark(location_t const l)
{
    assert(l <= m_text.size()); // including end sentinel

    return m_marks.add(l);
}

bool buffer::
remove_mark(mark const& m)
{
    return m_marks.remove(m);
}

void buffer::
shift_marks(location_t const loc, offset_t const n)
{
    m_marks.shift(loc, n);
}

void buffer::
move_mark_to(mark const& m, location_t const l)
{
    m_marks.move_to(m, l);
}

void buffer::
insert_text(location_t const loc, std::string_view const s)
{
    assert(loc <= m_text.size());

    if (s.empty()) return;

    length_t t_length = s.size();

    m_text.insert(loc, s);
    if (!m_text.empty() && m_text.back() != '\n') {
        m_text.push_back('\n');
        ++t_length;
    }

    auto lno = line_no(loc);
    auto bol = beginning_of_line(lno);
    auto need_to_shift_following_lines = bol < loc;

    for (auto i = loc; i < loc + t_length; ++i) {
        if (m_text[i] == '\n') {
            auto const eol = i + 1;
            auto const len = eol - bol;

            if (need_to_shift_following_lines) {
                m_lines.shift(lno + 1, eol - loc);
                need_to_shift_following_lines = false;
            }

            m_lines.insert(lno, bol, len);
            bol = eol;
            ++lno;
        }
    }

    if (need_to_shift_following_lines) {
        m_lines.shift(lno + 1, t_length);
    }

    m_marks.shift(loc, t_length);
}

void buffer::
replace_text(location_t const loc, length_t const len, std::string_view const s)
{
    m_text.replace(loc, len, s);

    auto const offset = offset_t { s.size() } - len;

    m_lines.shift(line_no(loc) + 1, offset);
    m_marks.shift(loc + 1, offset);
}

void buffer::
remove_text(location_t const loc, length_t const len)
{
    assert(loc + len <= m_text.size());

    if (len == 0) return;

    auto const end = loc + len;
    auto const lfrom = line_no(loc);
    auto const lto = line_no(end);

    m_text.erase(loc, len);

    m_lines.remove(lfrom, lto);

    m_marks.shift(end, -len);
}

void buffer::
clear()
{
    m_text.clear();
    m_lines.clear();
    m_marks.clear();
}

std::ostream&
operator<<(std::ostream& os, buffer const& buf)
{
    return os << buf.text();
}

} // namespace stream9::ini::editors
