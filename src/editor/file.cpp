#include <stream9/ini/editor/file.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <system_error>

//TODO test
//
namespace stream9::ini::editors {

static std::string
slurp_file(auto const& p)
{
    std::ostringstream oss;
    std::ifstream ifs { p };

    if (ifs.fail()) {
        std::error_code ec { errno, std::system_category() };
        if (ec != std::errc::no_such_file_or_directory) {
            throw std::system_error { ec };
        }
    }
    else {
        oss << ifs.rdbuf();
    }

    return oss.str();
}

// file

file::
file(path_t const& p)
    : editor { slurp_file(p) }
    , m_path { p }
{}

void file::
save()
{
    std::ofstream ofs { m_path };
    if (ofs.fail()) {
        throw std::system_error { errno, std::system_category() };
    }

    ofs << this->text();
}

} // namespace stream9::ini::editors
