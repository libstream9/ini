#include <stream9/ini/editor/entry.hpp>

#include <stream9/ini/escape.hpp>

#include <ostream>

namespace stream9::ini::editors {

location_t entry::
location() const
{
    return m_key.location();
}

void entry::
shift_mark(offset_t const o)
{
    m_key.shift_mark(o);
    m_value.shift_mark(o);
}

line_result
make_entry_line(std::string_view const key, std::string_view const value)
{
    if (key.empty()) {
        throw error { error_code::empty_key };
    }

    auto line = escape_key(key);
    auto const key_off = 0;
    auto const key_len = line.size();

    line.append(" = ");
    auto const value_off = line.size();

    line.append(escape_value(value));
    auto const value_len = line.size() - value_off;

    line.push_back('\n');

    return {
        std::move(line),
        key_off, key_len,
        value_off, value_len,
    };
}

std::ostream&
operator<<(std::ostream& os, entry const& e)
{
    return os << e.key() << " = " << e.value();
}

} // namespace stream9::ini::editors
