#include <stream9/ini/editor/editor.hpp>

#include <stream9/ini/editor/parser.hpp>
#include <stream9/ini/editor/section.hpp>

#include <stream9/ini/error.hpp>
#include <stream9/ini/escape.hpp>

#include <algorithm>
#include <fstream>
#include <istream>
#include <iterator>
#include <ostream>
#include <sstream>
#include <system_error>

namespace stream9::ini::editors {

static auto
find(auto& sections, auto const name)
{
    return std::find_if(sections.begin(), sections.end(),
        [&](auto& sptr) {
            return sptr->name() == name;
        } );
}

static index_t
index_of(auto const& sections, auto& section)
{
    auto const it = std::find_if(
        sections.begin(), sections.end(),
        [&](auto& ptr) {
            return ptr.get() == &section;
        });

    return std::distance(sections.begin(), it);
}

// editor

editor::
editor()
    : editor { "" }
{}

editor::
editor(std::string text)
    : m_buffer { std::make_unique<class buffer>(std::move(text)) }
    , m_sections { std::make_unique<set_t>() }
{
    parser p { *this };
    p.run();
}

editor::
editor(std::string text, std::vector<parse_error>& errors)
    : m_buffer { std::make_unique<class buffer>(std::move(text)) }
    , m_sections { std::make_unique<set_t>() }
{
    parser p { *this, &errors };
    p.run();
}

editor& editor::
operator=(editor&& other) noexcept
{
    // moving order is important.
    // m_buffer needs to exist when destruct m_sections
    m_sections = std::move(other.m_sections);
    m_buffer = std::move(other.m_buffer);

    return *this;
}

section* editor::
find_section(std::string_view const name)
{
    auto it = std::find_if(m_sections->begin(), m_sections->end(),
        [&](auto& sptr) {
            return sptr->name() == name;
        });

    return it == m_sections->end() ? nullptr : it->get();
}

section const* editor::
find_section(std::string_view const name) const
{
    return const_cast<section const*>(
        const_cast<editor*>(this)->find_section(name) );
}

section& editor::
find_section_or_throw(std::string_view const name)
{
    auto* const s = find_section(name);
    if (s) {
        return *s;
    }
    else {
        throw error { error_code::no_such_section };
    }
}

section const& editor::
find_section_or_throw(std::string_view const name) const
{
    return const_cast<section const&>(
        const_cast<editor*>(this)->find_section_or_throw(name) );
}

value* editor::
find_value(std::string_view const section, std::string_view const key)
{
    auto* const s = find_section(section);
    if (!s) return nullptr;

    return s->find(key);
}

value const* editor::
find_value(std::string_view const section, std::string_view const key) const
{
    return const_cast<value const*>(
        const_cast<editor*>(this)->find_value(section, key) );
}

std::string_view editor::
find_value(std::string_view const section,
           std::string_view const key, std::string_view const fallback) const
{
    auto* const s = find_section(section);
    if (!s) return fallback;

    auto* const v = s->find(key);
    if (!v) return fallback;

    return v->text();
}

value& editor::
find_value_or_throw(std::string_view const section, std::string_view const key)
{
    auto& s = find_section_or_throw(section);
    return s.find_or_throw(key);
}

value const& editor::
find_value_or_throw(std::string_view const section, std::string_view const key) const
{
    return const_cast<value const&>(
        const_cast<editor*>(this)->find_value_or_throw(section, key) );
}

bool editor::
contains_section(std::string_view const key) const
{
    return find_section(key) != nullptr;
}

std::string const& editor::
text() const
{
    return m_buffer->text();
}

void editor::
assign_text(std::string text)
{
    *this = editor { text };
}

section& editor::
create_section(std::string_view const name)
{
    if (contains_section(name)) {
        throw error { error_code::duplicate_section_name };
    }

    if (name.empty()) {
        auto anonymous = section::make(*this);
        m_sections->push_back(std::move(anonymous));

        if (section_count() > 1) {
            m_sections->back()->append_empty_line();
        }
    }
    else {
        auto lno = m_buffer->line_count();

        if (section_count() > 0) {
            m_sections->back()->append_empty_line();
            ++lno;
        }

        auto const& [line, loc, len] = make_section_line(name);

        auto const bol = m_buffer->beginning_of_line(lno);
        m_buffer->insert_text(bol, line);
        region const name_r { bol + loc, len };

        auto s = section::make(*this, name_r, bol);
        s->increment_line(); //TODO ugly

        m_sections->push_back(std::move(s));
    }

    return *m_sections->back();
}

section& editor::
find_or_create_section(std::string_view const name)
{
    auto* const s = find_section(name);
    if (s) {
        return *s;
    }
    else {
        return create_section(name);
    }
}

void editor::
move_section(section& s, index_t const to)
{
    assert(to < m_sections->size());

    if (s.name() == "") {
        throw error { error_code::cant_move_section };
    }

    if (to == 0 && contains_section("")) {
        throw error { error_code::cant_move_section };
    }

    auto const from = index_of(*m_sections, s);
    if (from == to) return;

    auto& dest_s = *m_sections->at(to);

    auto const dest = from < to ? dest_s.bottom() : dest_s.top();

    s.move_to(dest);

    auto const from_it = m_sections->begin() + from;
    auto s_ptr = std::move(*from_it);

    m_sections->erase(from_it);

    auto to_it = m_sections->begin() + to;
    m_sections->insert(to_it, std::move(s_ptr));
}

bool editor::
erase_section(std::string_view const name)
{
    auto const it = find(*m_sections, name);

    if (it == m_sections->end()) {
        return false;
    }
    else {
        (*it)->clear();

        m_sections->erase(it);
        return true;
    }
}

void editor::
clear()
{
    m_sections->clear();
    m_buffer->clear();
}

void editor::
read(std::istream& is)
{
    std::ostringstream oss;
    oss << is.rdbuf();

    assign_text(oss.str());
}

void editor::
write(std::ostream& os)
{
    os << *m_buffer;
}

section& editor::
append_section(std::unique_ptr<section>&& s)
{
    m_sections->push_back(std::move(s));
    return *m_sections->back();
}

std::ostream&
operator<<(std::ostream& os, editor const& s)
{
    return os << s.buffer();
}

std::istream&
operator>>(std::istream& is, editor& e)
{
    e.read(is);
    return is;
}

editor
load_from_file(std::filesystem::path const& p)
{
    std::ifstream ifs;
    ifs.open(p);
    if (ifs.fail()) {
        throw std::system_error { errno, std::system_category() };
    }

    std::ostringstream oss;

    oss << ifs.rdbuf();
    if (ifs.fail() || oss.fail()) {
        throw std::system_error { errno, std::system_category() };
    }

    return editor { oss.str() };
}

void
save_to_file(editor const& e, std::filesystem::path const& p)
{
    std::ofstream ofs;
    ofs.open(p);
    if (ofs.fail()) {
        throw std::system_error { errno, std::system_category() };
    }

    ofs << e;
    if (ofs.fail()) {
        throw std::system_error { errno, std::system_category() };
    }
}

} // namespace stream9::ini::editors
