#include <stream9/ini/editor/mark.hpp>

#include <stream9/ini/editor/mark_set.hpp>

namespace stream9::ini::editors {

mark::
mark(class mark_set& set, location_t const l)
    : m_mark_set { &set }
    , m_location { l }
{}

mark::~mark() noexcept = default;

void mark::
shift(offset_t const offset)
{
    auto const loc = m_location + offset;
    m_mark_set->move_to(*this, loc);
}

void mark::
move_to(location_t const loc)
{
    m_mark_set->move_to(*this, loc);
}

std::ostream&
operator<<(std::ostream& os, mark const& m)
{
    return os << m.location();
}

void mark_deleter::
operator()(mark* const m) const
{
    auto const ok = m->mark_set().remove(*m);
    assert(ok);
}

} // namespace stream9::ini::editors
