#include <stream9/ini/editor/parser.hpp>

#include <stream9/ini/editor/editor.hpp>
#include <stream9/ini/editor/section.hpp>

namespace stream9::ini::editors {

// parser

parser::
parser(editor& e, std::vector<parse_error>* errors/*= nullptr*/)
    : m_editor { e }
    , m_text { m_editor.buffer().text() }
    , m_bol { m_text.begin() }
    , m_errors { errors }
{}

void parser::
run()
{
    ini::scan(m_text, *this);

    if (m_current && m_line_no == 1) { // there isn't new line in m_text
        m_current->increment_line();
    }
}

void parser::
on_section(std::string_view const name)
{
    assert(!name.empty());

    try {
        auto const loc = get_location(name);
        region const name_r { loc, name.size() };

        auto const top = m_bol - m_text.data();

        auto s = section::make(m_editor, name_r, top);

        m_current = &m_editor.append_section(std::move(s));

        m_skip_section = false;
    }
    catch (error const& e) {
        m_skip_section = true;

        location_t const loc = name.data() - m_bol;
        handle_error(e.code(), loc);
    }
}

void parser::
on_entry(std::string_view const key,
         std::string_view const value)
{
    if (m_skip_section) return;

    if (m_current == nullptr) {
        m_current = &m_editor.create_section("");
    }

    try {
        auto const key_loc = get_location(key);
        auto const value_loc = get_location(value);

        region const key_r { key_loc, key.size() };
        region const value_r { value_loc, value.size() };

        m_current->append(key_r, value_r);
    }
    catch (error const& e) {
        location_t const loc = key.data() - m_bol;
        handle_error(e.code(), loc);
    }
}

void parser::
on_newline(std::string_view const content)
{
    assert(!content.empty());

    if (m_current == nullptr) {
        m_current = &m_editor.create_section("");
    }

    m_current->increment_line();

    ++m_line_no;
    m_bol = content.end();
}

location_t parser::
get_location(std::string_view const range) const
{
    return range.data() - m_text.data();
}

void parser::
on_error(error_code const code, std::string_view const range)
{
    using e = error_code;
    if (code == e::section_name_is_expected ||
        code == e::section_terminator_is_expected ||
        code == e::invalid_section_name)
    {
        m_skip_section = true;
    }

    location_t const loc = range.data() - m_bol;
    handle_error(code, loc);
}

void parser::
handle_error(error_code const code, location_t const loc) const
{
    parse_error ex {
        code,
        m_line_no,
        loc,
        current_line(),
        {} //TODO
    };

    if (m_errors) {
        m_errors->push_back(std::move(ex));
    }
    else {
        throw ex;
    }
}

std::string_view parser::
current_line() const
{
    auto it = m_bol;
    for (; it != m_text.end(); ++it) {
        if (*it == '\n') break; //TODO CRLF
    }

    return { m_bol, it };
}

} // namespace stream9::ini::editors
