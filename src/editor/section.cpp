#include <stream9/ini/editor/section.hpp>

#include <stream9/ini/editor/buffer.hpp>
#include <stream9/ini/editor/editor.hpp>
#include <stream9/ini/editor/region.hpp>

#include <stream9/ini/error.hpp>
#include <stream9/ini/escape.hpp>

#include <algorithm>
#include <ostream>

namespace stream9::ini::editors {

// section

section::
section(editor& e)
    : section { e, 0 }
{}

section::
section(editor& e, class region const& name, location_t const top)
    : section { e, top }
{
    m_name = section_name { buffer(), name };
}

section::
section(editor& e, location_t const top)
    : m_editor { &e }
    , m_top { buffer().add_mark(top) }
{}

std::string_view section::
name() const
{
    if (m_name) {
        return *m_name;
    }
    else {
        return "";
    }
}

location_t section::
head() const
{
    if (!m_name) {
        return top();
    }
    else {
        auto const top_line = buffer().line_no(top());
        return buffer().beginning_of_line(top_line + 1);
    }
}

location_t section::
bottom() const
{
    auto const line_from = buffer().line_no(top());
    auto const line_to = line_from + m_num_lines;
    return buffer().beginning_of_line(line_to);
}

value const* section::
find(std::string_view const key) const
{
    auto it = std::find_if(m_entries.begin(), m_entries.end(),
        [&](auto& entry) {
            return entry.key() == key;
        });

    return it == m_entries.end() ? nullptr : &it->value();
}

value* section::
find(std::string_view const key)
{
    return const_cast<value*>(
        const_cast<section const*>(this)->find(key) );
}

std::string_view section::
find(std::string_view const key, std::string_view const fallback) const
{
    auto* const v = find(key);
    if (v) {
        return *v;
    }
    else {
        return fallback;
    }
}

value const& section::
find_or_throw(std::string_view const key) const
{
    auto it = std::find_if(m_entries.begin(), m_entries.end(),
        [&](auto& entry) {
            return entry.key() == key;
        });

    if (it == m_entries.end()) {
        throw error { error_code::no_such_key };
    }
    else {
        return it->value();
    }
}

value& section::
find_or_throw(std::string_view const key)
{
    return const_cast<value&>(
        const_cast<section const*>(this)->find_or_throw(key) );
}

bool section::
contains(std::string_view const key) const
{
    return find(key) != nullptr;
}

std::string_view section::
text() const
{
    auto const len = bottom() - top();
    return buffer().text({ top(), len });
}

void section::
append(std::string_view const key, std::string_view const value)
{
    if (key.empty()) {
        throw error { error_code::empty_key };
    }
    if (find(key)) {
        throw error { error_code::duplicate_key };
    }

    auto const& line = make_entry_line(key, value);

    auto const head_lno = buffer().line_no(head());
    auto const tail_lno = head_lno + m_entries.size();
    auto const bol = buffer().beginning_of_line(tail_lno);
    auto const need_adjust_top = tail_lno == 0;

    buffer().insert_text(bol, line.line);

    region const key_r { bol + line.key_off, line.key_len };
    region const value_r { bol + line.value_off, line.value_len };

    class key k { buffer(), key_r };
    class value v { buffer(), value_r };

    m_entries.emplace_back(std::move(k), std::move(v));

    if (need_adjust_top) {
        length_t const line_len = line.line.size();
        m_top->shift(-line_len);
    }

    ++m_num_lines;
}

value& section::
find_or_append(std::string_view const key)
{
    if (key.empty()) {
        throw error { error_code::empty_key };
    }

    auto* const value = find(key);
    if (value) return *value;

    append(key, "");
    return m_entries.back().value();
}

bool section::
erase(std::string_view const key)
{
    auto const it = std::find_if(m_entries.begin(), m_entries.end(),
        [&](auto& e) {
            return e.key() == key;
        } );

    if (it == m_entries.end()) return false;

    auto const loc = it->location();
    auto const line_no = buffer().line_no(loc);
    auto const bol = buffer().beginning_of_line(line_no);
    auto const eol = buffer().beginning_of_line(line_no + 1);

    buffer().remove_text(bol, eol - bol);

    m_entries.erase(it);

    --m_num_lines;

    return true;
}

void section::
rename(std::string_view const n)
{
    if (n == name()) return;

    if (m_editor->contains_section(n)) {
        throw error { error_code::duplicate_section_name };
    }

    if (n.empty()) {
        m_editor->move_section(*this, 0);

        auto const bol = top();
        auto const eol = head();
        buffer().remove_text(bol, eol - bol);
        --m_num_lines;

        m_name = std::nullopt;
    }
    else {
        if (m_name) {
            *m_name = n;
        }
        else {
            auto const& [line, name_loc, name_len] = make_section_line(n);
            buffer().insert_text(top(), line);
            ++m_num_lines;

            length_t const line_len = line.size();
            m_top->shift(-line_len);

            m_name = section_name { buffer(), { name_loc, name_len } };
        }
    }
}

void section::
clear()
{
    if (m_entries.empty()) return;

    auto const from = top();
    auto const len = bottom() - from;
    buffer().remove_text(from, len);

    m_entries.clear();

    m_num_lines = 0;
}

std::unique_ptr<section> section::
make(editor& e)
{
    return std::unique_ptr<section> { new section { e } };
}

std::unique_ptr<section> section::
make(editor& e, class region const& name, location_t const top)
{
    return std::unique_ptr<section> { new section { e, name, top } };
}

class buffer& section::
buffer()
{
    return m_editor->buffer();
}

class buffer const& section::
buffer() const
{
    return m_editor->buffer();
}

void section::
append(class region const& key_region, class region const& value_region)
{
    m_entries.emplace_back(
        key { buffer(), key_region },
        value { buffer(), value_region } );
}

void section::
move_to(location_t const dest)
{
    buffer().insert_text(dest, text());

    auto const top_before_shifted = top();
    auto const offset = dest - top();

    m_top->shift(offset);

    for (auto& e: m_entries) {
        e.shift_mark(offset);
    }

    buffer().remove_text(top_before_shifted, text().size());
}

void section::
append_empty_line()
{
    auto const lno = buffer().line_no(top()) + m_num_lines;

    auto const need_adjust_top = lno == 0;

    buffer().insert_text(buffer().beginning_of_line(lno), "\n");

    if (need_adjust_top) {
        m_top->shift(-1);
    }

    ++m_num_lines;
}

std::ostream&
operator<<(std::ostream& os, section const& s)
{
    return os << s.text();
}

section_line
make_section_line(std::string_view const name)
{
    std::string line = "[";
    auto const& escaped = escape_section(name);
    line.append(escaped);
    line.append("]\n");

    return { std::move(line), 1, escaped.size() };
}

} // namespace stream9::ini::editors
