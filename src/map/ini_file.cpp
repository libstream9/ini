#include <stream9/ini/map/ini_file.hpp>

#include <stream9/ini/scanner.hpp>
#include <stream9/ini/escape.hpp>

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

namespace stream9::ini {

static std::string
slurp(std::filesystem::path const& path)
{
    std::ifstream ifs { path.c_str() };
    std::ostringstream oss;

    oss << ifs.rdbuf();

    return oss.str();
}

// parser

class parser : public event_handler
{
public:
    parser(std::string_view const text,
           ini_file& file,
           std::vector<parse_error>* errors = nullptr)
        : m_text { text }
        , m_file { file }
        , m_current { &m_file.default_section() }
        , m_bol { m_text.begin() }
        , m_errors { errors }
    {}

    void run()
    {
        ini::scan(m_text, *this);
    }

    std::string_view current_line() const
    {
        auto it = m_bol;
        for (; it != m_text.end(); ++it) {
            if (*it == '\n') break;
        }

        return { m_bol, it };
    }

    void handle_error(error_code const code, int const pos)
    {
        parse_error ex {
            code,
            m_line_no,
            pos,
            current_line(),
            m_file.path()
        };

        if (m_errors) {
            m_errors->push_back(std::move(ex));
        }
        else {
            throw ex;
        }
    }

    // override event_handler
    void on_section(std::string_view const name) override
    {
        assert(!name.empty());

        try {
            auto const n = unescape(name);
            auto& section = m_file.create_section(unquote(n));

            m_current = &section;
            m_skip_section = false;
        }
        catch (error const& e) {
            m_skip_section = true;

            auto const pos = static_cast<int>(name.begin() - m_bol);
            handle_error(e.code(), pos);
        }
    }

    void on_entry(std::string_view const key,
                  std::string_view const value) override
    {
        if (m_skip_section) return;

        try {
            auto const k = unescape(key);

            auto v = unescape_continuation(value);
            v = unescape(v);

            m_current->append(unquote(k), unquote(v));
        }
        catch (error const& e) {
            auto const pos = static_cast<int>(key.begin() - m_bol);
            handle_error(e.code(), pos);
        }
    }

    void on_newline(std::string_view const content) override
    {
        assert(!content.empty());

        ++m_line_no;
        m_bol = content.end();
    }

    void on_error(error_code const code, std::string_view const range) override
    {
        using e = error_code;
        if (code == e::section_name_is_expected ||
            code == e::section_terminator_is_expected ||
            code == e::invalid_section_name)
        {
            m_skip_section = true;
        }

        auto const pos = static_cast<int>(range.begin() - m_bol);
        handle_error(code, pos);
    }

private:
    std::string_view m_text;
    ini_file& m_file;
    section* m_current; // non-null
    int m_line_no = 1;
    std::string_view::const_iterator m_bol;
    std::vector<parse_error>* m_errors = nullptr;
    bool m_skip_section = false;
};

// ini_file

ini_file::
ini_file(fs::path const& path)
    : m_path { path }
{
    m_sections.emplace_back("");

    auto const& text = slurp(m_path);

    parser p { text, *this };
    p.run();
}

ini_file::
ini_file(fs::path path, std::vector<parse_error>& errors)
    : m_path { std::move(path) }
{
    m_sections.emplace_back("");

    auto const& text = slurp(m_path);

    parser p { text, *this, &errors };
    p.run();
}

section const* ini_file::
find_section(std::string_view const name) const
{
    auto const it = std::find_if(
        m_sections.begin(), m_sections.end(),
        [&](auto& s) {
            return s.name() == name;
        } );

    if (it == m_sections.end()) return nullptr;

    return &*it;
}

section* ini_file::
find_section(std::string_view const name)
{
    return const_cast<section*>(
        const_cast<ini_file const*>(this)->find_section(name) );
}

section const& ini_file::
default_section() const
{
    auto* const s = find_section("");
    assert(s);

    return *s;
}

section& ini_file::
default_section()
{
    return const_cast<section&>(
        const_cast<ini_file const*>(this)->default_section() );
}

bool ini_file::
contains(std::string_view const key) const
{
    return find_section(key) != nullptr;
}

section& ini_file::
create_section(std::string_view const name)
{
    if (find_section(name)) {
        throw error { error_code::duplicate_section_name };
    }

    m_sections.emplace_back(name);

    return m_sections.back();
}

section& ini_file::
find_or_create_section(std::string_view const name)
{
    auto* const s = find_section(name);
    if (s) {
        return *s;
    }
    else {
        return create_section(name);
    }
}

bool ini_file::
erase_section(std::string_view const name)
{
    if (name.empty()) return false;

    auto const it = std::remove_if(m_sections.begin(), m_sections.end(),
        [&](auto& s) {
            return s.name() == name;
        } );

    if (it == m_sections.end()) return false;

    m_sections.erase(it, m_sections.end());

    return true;
}

void ini_file::
clear()
{
    m_sections.clear();
    m_sections.emplace_back("");
}

void ini_file::
save()
{
    auto const& before = slurp(m_path);

    std::ostringstream oss;
    oss << *this;

    auto const& after = oss.str();

    if (before != after) {
        std::ofstream ofs { m_path.c_str() };

        ofs << after;
    }
}

void ini_file::
reload()
{
    clear();

    auto const& text = slurp(m_path);

    parser p { text, *this };
    p.run();
}

// free functions

parse_result
parse(fs::path path)
{
    std::vector<parse_error> errors;

    ini_file ini { std::move(path), errors };

    return { std::move(ini), std::move(errors) };
}

std::ostream&
operator<<(std::ostream& os, ini_file const& ini)
{
    for (auto const& section: ini) {
        if (section.empty()) continue;

        os << section;
    }

    return os;
}

} // namespace stream9::ini
