#include <stream9/ini/map/section.hpp>

#include <stream9/ini/error.hpp>
#include <stream9/ini/escape.hpp>

#include <algorithm>
#include <cassert>
#include <ostream>

namespace stream9::ini {

using const_iterator = section::const_iterator;

section::
section(std::string_view const name)
    : m_name { name }
{}

value_t const* section::
find(std::string_view const key) const
{
    auto const it = std::find_if(m_map.begin(), m_map.end(),
        [&](auto& e) {
            return e.key == key;
        } );

    if (it == m_map.end()) return nullptr;

    return &it->value;
}

value_t* section::
find(std::string_view const key)
{
    return const_cast<value_t*>(
        const_cast<section const*>(this)->find(key) );
}

bool section::
contains(std::string_view const key) const
{
    return find(key) != nullptr;
}

void section::
append(std::string_view const key, std::string_view const value)
{
    if (key.empty()) {
        throw error { error_code::empty_key };
    }
    if (find(key)) {
        throw error { error_code::duplicate_key };
    }

    entry e { std::string(key), std::string(value) };
    m_map.push_back(std::move(e));
}

value_t& section::
find_or_append(std::string_view const key)
{
    if (key.empty()) {
        throw error { error_code::empty_key };
    }

    auto* const value = find(key);
    if (value) return *value;

    append(key, "");
    return m_map.back().value;
}

bool section::
erase(std::string_view const key)
{
    auto const it = std::remove_if(m_map.begin(), m_map.end(),
        [&](auto& e) {
            return e.key == key;
        } );

    if (it == m_map.end()) return false;

    m_map.erase(it, m_map.end());

    return true;
}

void section::
clear()
{
    m_map.clear();
}

std::ostream&
operator<<(std::ostream& os, entry const& e)
{
    return os << escape_key(e.key) << " = " << escape_value(e.value);
}

std::ostream&
operator<<(std::ostream& os, section const& s)
{
    if (s.empty()) return os;

    os << '[' << escape_section(s.name()) << "]\n";

    for (auto const& entry: s) {
        os << entry << '\n';
    }

    return os;
}

} // namespace stream9::ini
