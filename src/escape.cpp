#include <stream9/ini/escape.hpp>

#include <stream9/ini/error.hpp>

#include <cassert>
#include <charconv>
#include <climits>
#include <cuchar>
#include <iterator>

namespace stream9::ini {

static void
decode_utf16_character(std::string& s,
                      char const* const it, char const* end)
{
    if (std::distance(it, end) < 4) {
        throw unknown_escape_sequence({ it - 2, it });
    }

    int c16 = 0;
    auto [ptr, ec] = std::from_chars(it, it + 4, c16, 16);
    if (ec != std::errc() || ptr != it + 4) {
        throw unknown_escape_sequence({ it - 2, ptr + 1 });
    }
    char mb[MB_LEN_MAX] {};
    std::mbstate_t state {};

    auto const n = std::c16rtomb(mb, static_cast<char16_t>(c16), &state);
    if (n == static_cast<size_t>(-1)) {
        throw unknown_escape_sequence({ it - 2, it + 3 });
    }

    for (size_t i = 0; i < n; ++i) {
        s.push_back(mb[i]);
    }
}

static char const*
escape_to(char const c)
{
    switch (c) {
        case '\\':
            return "\\\\";
        case '\'':
            return "\\'";
        case '\"':
            return "\\\"";
        case '\0':
            return "\\0";
        case '\a':
            return "\\a";
        case '\b':
            return "\\b";
        case '\t':
            return "\\t";
        case '\r':
            return "\\r";
        case '\n':
            return "\\n";
        case ';':
            return "\\;";
        case '#':
            return "\\#";
    }

    return nullptr;
}

static char const*
escape_ctl_quote(char const c)
{
    switch (c) {
        case '\\':
            return "\\\\";
        case '\'':
            return "\\'";
        case '\"':
            return "\\\"";
        case '\0':
            return "\\0";
        case '\a':
            return "\\a";
        case '\b':
            return "\\b";
        case '\t':
            return "\\t";
        case '\r':
            return "\\r";
        case '\n':
            return "\\n";
    }

    return nullptr;
}

static char const*
escape_comment_mark(char const c)
{
    switch (c) {
        case ';':
            return "\\;";
        case '#':
            return "\\#";
    }

    return nullptr;
}

static bool
is_space(char const c)
{
    return c == ' ' || c == '\t';
}

std::string
escape_section(std::string_view const s)
{
    std::string result;

    bool need_quote = false;

    for (auto const c: s) {
        auto* const s = escape_ctl_quote(c);
        if (s) {
            result.append(s);
        }
        else if (auto* const s = escape_comment_mark(c)) {
            result.push_back(c);
            need_quote = true;
        }
        else if (c == ']') {
            result.push_back(c);
            need_quote = true;
        }
        else {
            result.push_back(c);
        }
    }

    if (!result.empty()) {
        if (is_space(result.front()) || is_space(result.back())) {
            need_quote = true;
        }

        if (need_quote) {
            result.insert(result.begin(), '"');
            result.push_back('"');
        }
    }

    return result;
}

std::string
escape_key(std::string_view const text)
{
    std::string result;

    if (text.empty()) return result;

    for (auto it = text.begin(); it != text.end(); ++it) {
        auto const c = *it;
        auto* const s = escape_to(c);
        if (s) {
            result.append(s);
        }
        else if (c == '=') {
            result.append("\\=");
        }
        else if (it == text.begin() &&  c == '[') {
            result.append("\\[");
        }
        else {
            result.push_back(c);
        }
    }

    return result;
}

std::string
escape_value(std::string_view const s)
{
    std::string result;

    for (auto const c: s) {
        auto* const s = escape_to(c);
        if (s) {
            result.append(s);
        }
        else {
            result.push_back(c);
        }
    }

    return result;
}

std::string
unescape(std::string_view const s)
{
    std::string result;

    result.reserve(s.size());

    for (auto it = s.begin(); it != s.end(); ++it) {
        if (*it != '\\') {
            result.push_back(*it);
            continue;
        }

        ++it;
        if (it == s.end()) {
            throw unknown_escape_sequence({ it - 1, it });
        }

        auto const c = *it;

        switch (c) {
            case '\\':
            case '\'':
            case '\"':
                result.push_back(c); break;
            case '0':
                result.push_back('\0'); break;
            case 'a':
                result.push_back('\a'); break;
            case 'b':
                result.push_back('\b'); break;
            case 't':
                result.push_back('\t'); break;
            case 'r':
                result.push_back('\r'); break;
            case 'n':
                result.push_back('\n'); break;
            case ';':
            case '#':
            case '=':
            case '[':
            case ']':
                result.push_back(c); break;
            case 'x':
                decode_utf16_character(result, it + 1, s.end());
                it += 4;
                break;
            default:
                throw unknown_escape_sequence({ it - 1, it + 1 });
        }
    }

    return result;
}

std::string
unescape_continuation(std::string_view const s)
{
    std::string result;

    for (auto it = s.begin(); it != s.end(); ++it) {
        if (*it != '\\') {
            result.push_back(*it);
            continue;
        }

        auto const next = it + 1;

        if (next != s.end() && *next == '\n') {
            it = next;
        }
        else {
            result.push_back(*it);
        }
    }

    return result;
}

static bool
is_quote_mark(char const c)
{
    return c == '\'' || c == '"';
}

std::string_view
unquote(std::string_view const s)
{
    if (s.empty()) return s;

    auto front = s.front();

    if (is_quote_mark(front)) {
        if (s.size() == 1) {
            return s;
        }
        else if (s.back() == front) {
            return { s.begin() + 1, s.end() - 1 };
        }
        else {
            return s;
        }
    }
    else {
        return s;
    }
}

} // namespace stream9::ini
