#ifndef STREAM9_INI_INI_HPP
#define STREAM9_INI_INI_HPP

#include "section.hpp"

#include "../error.hpp"
#include "../type.hpp"

#include <filesystem>
#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

namespace stream9::ini {

namespace fs = std::filesystem;

class ini_file
{
public:
    explicit ini_file(fs::path const& path);
    ini_file(fs::path, std::vector<parse_error>&);

    ini_file(ini_file const&) = delete;
    ini_file& operator=(ini_file const&) = delete;

    ini_file(ini_file&&) = default;
    ini_file& operator=(ini_file&&) = default;

    // accessor
    fs::path const& path() const { return m_path; }

    // query
    section const* find_section(std::string_view name) const;
    section* find_section(std::string_view name);

    section const& default_section() const;
    section& default_section();

    bool contains(std::string_view key) const;
    size_t size() const { return m_sections.size(); }

    // modifier
    section& create_section(std::string_view name);
    section& find_or_create_section(std::string_view name);
    bool erase_section(std::string_view name);

    // command
    void clear();

    void save();
    void reload();

    // iterator
    auto begin() const { return m_sections.begin(); }
    auto end() const { return m_sections.end(); }

    // operator
    section& operator[](std::string_view name) { return find_or_create_section(name); }

private:
    fs::path m_path;
    std::vector<section> m_sections;
};

// free functions

struct parse_result {
    ini_file ini;
    std::vector<parse_error> errors;
};

parse_result parse(fs::path);

std::ostream& operator<<(std::ostream&, ini_file const&);

} // namespace stream9::ini

#endif // STREAM9_INI_INI_HPP
