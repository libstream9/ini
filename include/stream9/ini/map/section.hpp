#ifndef STREAM9_INI_SECTION_HPP
#define STREAM9_INI_SECTION_HPP

#include "../type.hpp"

#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

namespace stream9::ini {

struct entry
{
    std::string key;
    std::string value;
};

class section
{
public:
    using map_t = std::vector<entry>;
    using const_iterator = map_t::const_iterator;

public:
    section(std::string_view name);

    // accessor
    std::string const& name() const { return m_name; }

    // query
    value_t const* find(std::string_view key) const;
    value_t* find(std::string_view key);

    bool contains(std::string_view key) const;
    size_t size() const { return m_map.size(); }
    bool empty() const { return m_map.empty(); }

    // modifier
    void append(std::string_view key, std::string_view value);
    value_t& find_or_append(std::string_view key);

    bool erase(std::string_view key);

    // command
    void clear();

    // iterator
    const_iterator begin() const { return m_map.begin(); }
    const_iterator end() const { return m_map.end(); }

    // operator
    value_t& operator[](std::string_view key) { return find_or_append(key); }

private:
    std::string m_name;
    map_t m_map;
};

std::ostream& operator<<(std::ostream&, entry const&);
std::ostream& operator<<(std::ostream&, section const&);

} // namespace stream9::ini

#endif // STREAM9_INI_SECTION_HPP
