#ifndef STREAM9_INI_EDITOR_REGION_HPP
#define STREAM9_INI_EDITOR_REGION_HPP

#include "type.hpp"

namespace stream9::ini::editors {

class region
{
public:
    region(location_t, length_t);

    // accessor
    location_t location() const { return m_location; }
    length_t length() const { return m_length; }

private:
    location_t m_location;
    length_t m_length;
};

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_REGION_HPP
