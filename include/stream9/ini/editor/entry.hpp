#ifndef STREAM9_INI_EDITORS_ENTRY_HPP
#define STREAM9_INI_EDITORS_ENTRY_HPP

#include "element.hpp"
#include "type.hpp"

#include <iosfwd>

namespace stream9::ini::editors {

class entry
{
public:
    entry(class key&& k, class value&& v)
        : m_key { std::move(k) }
        , m_value { std::move(v) }
    {}

    // accessor
    class key const& key() const { return m_key; }
    class value const& value() const { return m_value; }
    class value& value() { return m_value; }

    // query
    location_t location() const;

private:
    friend class section;

    void shift_mark(offset_t);

private:
    class key m_key;
    class value m_value;
};

struct line_result {
    std::string line;
    offset_t key_off;
    length_t key_len;
    offset_t value_off;
    length_t value_len;
};
line_result make_entry_line(std::string_view key, std::string_view value);

std::ostream& operator<<(std::ostream&, entry const&);

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITORS_ENTRY_HPP
