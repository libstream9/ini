#ifndef STREAM9_INI_EDITOR_LINE_INDEX_HPP
#define STREAM9_INI_EDITOR_LINE_INDEX_HPP

#include "type.hpp"

#include <string_view>
#include <vector>

namespace stream9::ini::editors {

class line_index
{
public:
    line_index(std::string_view text);

    // query
    count_t count() const;
    line_no_t line_no(location_t) const;
    location_t beginning_of_line(line_no_t) const;

    // modifier
    void insert(line_no_t, location_t, length_t);
    void remove(line_no_t from, line_no_t to);
    void shift(line_no_t, offset_t);
    void clear();

private:
    std::vector<location_t> m_lines;
};

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_LINE_INDEX_HPP
