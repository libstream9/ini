#ifndef STREAM9_INI_EDITOR_MARK_HPP
#define STREAM9_INI_EDITOR_MARK_HPP

#include "type.hpp"

#include <compare>
#include <iosfwd>
#include <memory>

namespace stream9::ini::editors {

class mark_set;

class mark
{
public:
    mark(class mark_set&, location_t);
    ~mark() noexcept;

    // accessor
    class mark_set& mark_set() { return *m_mark_set; }
    class mark_set& mark_set() const { return *m_mark_set; }

    location_t location() const { return m_location; }

    // modifier
    void shift(offset_t);
    void move_to(location_t);

    // comparison
    bool operator==(mark const&) const = default;
    auto operator<=>(mark const&) const = default;

    bool operator==(location_t const l) const { return m_location == l; }

    operator location_t () const { return m_location; }

private:
    friend class mark_set;

    void set_location(location_t const l) { m_location = l; }

private:
    class mark_set* m_mark_set; // non-null
    location_t m_location;
};

std::ostream& operator<<(std::ostream&, mark const&);

struct mark_deleter
{
    void operator()(mark* const m) const;
};

using unique_mark_ptr = std::unique_ptr<mark, mark_deleter>;

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_MARK_HPP
