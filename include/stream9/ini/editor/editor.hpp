#ifndef STREAM9_INI_EDITOR_EDITOR_HPP
#define STREAM9_INI_EDITOR_EDITOR_HPP

#include "buffer.hpp"
#include "section.hpp"
#include "type.hpp"

#include "../error.hpp"

#include <filesystem>
#include <iosfwd>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

namespace stream9::ini::editors {

class editor
{
public:
             editor();
    explicit editor(std::string text);
             editor(std::string text, std::vector<parse_error>&);

    editor(editor const&) = delete;
    editor& operator=(editor const&) = delete;

    editor(editor&&) = default;
    editor& operator=(editor&&) noexcept;

    // accessor
    class buffer&       buffer() { return *m_buffer; }
    class buffer const& buffer() const { return *m_buffer; }

    // query
    section*       find_section(std::string_view name);
    section const* find_section(std::string_view name) const;

    section&       find_section_or_throw(std::string_view name);
    section const& find_section_or_throw(std::string_view name) const;

    value*       find_value(std::string_view section, std::string_view key);
    value const* find_value(std::string_view section, std::string_view key) const;

    std::string_view find_value(std::string_view section,
                        std::string_view key, std::string_view fallback) const;

    value&       find_value_or_throw(std::string_view section, std::string_view key);
    value const& find_value_or_throw(std::string_view section, std::string_view key) const;

    bool contains_section(std::string_view key) const;
    count_t section_count() const { return m_sections->size(); }
    bool empty() const { return m_sections->empty(); }

    std::string const& text() const;

    // modifier
    void assign_text(std::string text);

    section& create_section(std::string_view name);

    section& find_or_create_section(std::string_view name);

    void move_section(section&, index_t);

    bool erase_section(std::string_view name);

    // command
    void clear();

    void read(std::istream&);
    void write(std::ostream&);

    // iterator
    auto begin() const { return m_sections->begin(); }
    auto end() const { return m_sections->end(); }

    // operator
    editor& operator=(std::string text) { assign_text(text); return *this; }

    section&       operator[](std::string_view name) { return find_or_create_section(name); }
    section const& operator[](std::string_view name) const { return find_section_or_throw(name); }

    section&       operator[](size_t const i) { return *(*m_sections)[i]; }
    section const& operator[](size_t const i) const { return *(*m_sections)[i]; }

private:
    friend class parser;
    section& append_section(std::unique_ptr<section>&&);

private:
    using set_t = std::vector<std::unique_ptr<section>>;

    std::unique_ptr<class buffer> m_buffer; // non-null
    std::unique_ptr<set_t> m_sections; // non-null
};

std::ostream& operator<<(std::ostream&, editor const&);
std::istream& operator>>(std::istream&, editor&);

editor load_from_file(std::filesystem::path const&);
void save_to_file(editor const&, std::filesystem::path const&);

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_EDITOR_HPP
