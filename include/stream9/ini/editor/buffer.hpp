#ifndef STREAM9_INI_EDITORS_BUFFER_HPP
#define STREAM9_INI_EDITORS_BUFFER_HPP

#include "line_index.hpp"
#include "mark_set.hpp"
#include "type.hpp"

#include <iosfwd>
#include <string>
#include <string_view>

namespace stream9::ini::editors {

class region;

class buffer
{
public:
    buffer();
    buffer(std::string text);

    // accessor
    std::string const& text() const { return m_text; }

    // query
    count_t size() const { return m_text.size(); }

    std::string_view text(region const&) const;

    count_t line_count() const;

    line_no_t line_no(location_t) const;
    line_no_t line_no(mark const&) const;

    location_t beginning_of_line(line_no_t) const;

    bool contains_mark(mark const&) const;
    bool empty() const { return m_text.empty(); }

    // modifier
    unique_mark_ptr add_mark(location_t);
    bool remove_mark(mark const&);
    void shift_marks(location_t, offset_t);
    void move_mark_to(mark const&, location_t);

    void insert_text(location_t, std::string_view);

    void replace_text(location_t, length_t, std::string_view);

    void remove_text(location_t, length_t);

    void clear();

private:
    std::string m_text;
    line_index m_lines;
    mark_set m_marks;
};

std::ostream& operator<<(std::ostream&, buffer const&);

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITORS_BUFFER_HPP
