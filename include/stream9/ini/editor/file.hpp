#ifndef STREAM9_INI_EDITOR_FILE_HPP
#define STREAM9_INI_EDITOR_FILE_HPP

#include "editor.hpp"

#include <filesystem>

namespace stream9::ini::editors {

class file : public editor
{
public:
    using path_t = std::filesystem::path;

public:
    file(path_t const&);

    //accessor
    path_t const& path() const { return m_path; }

    // command
    void save();

private:
    path_t m_path;
};

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_FILE_HPP

