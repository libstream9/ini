#ifndef STREAM9_INI_EDITOR_TYPE_HPP
#define STREAM9_INI_EDITOR_TYPE_HPP

#include "../type.hpp"

#include <stream9/safe_integer/safe_integer.hpp>

namespace stream9::ini::editors {

using location_t = safe_integers::safe_integer<int64_t, 0>;
using index_t = safe_integers::safe_integer<int64_t, 0>;
using length_t = safe_integers::safe_integer<int64_t, 0>;
using count_t = safe_integers::safe_integer<int64_t, 0>;
using line_no_t = safe_integers::safe_integer<int64_t, 0>;
using offset_t = safe_integers::safe_integer<int64_t>;

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_TYPE_HPP
