#ifndef STREAM9_INI_EDITOR_MARK_SET_HPP
#define STREAM9_INI_EDITOR_MARK_SET_HPP

#include "mark.hpp"
#include "type.hpp"

#include <memory>

#include <boost/container/flat_set.hpp>

namespace stream9::ini::editors {

class mark_set
{
public:
    mark_set() = default;

    // query
    bool contains(mark const&) const;

    // modifier
    unique_mark_ptr add(location_t);

    bool remove(mark const&);
    void remove(location_t from, location_t to);

    void shift(location_t, offset_t);

    void move_to(mark const&, location_t);

    void clear();

private:
    using mark_ptr = std::unique_ptr<mark>;

    struct comp {
        using is_transparent = void;

        bool operator()(mark_ptr const& lhs, mark_ptr const& rhs) const;
        bool operator()(mark_ptr const& lhs, mark const& rhs) const;
        bool operator()(mark const& lhs, mark_ptr const& rhs) const;
        bool operator()(mark_ptr const& lhs, location_t const& rhs) const;
        bool operator()(location_t const& lhs, mark_ptr const& rhs) const;
    };

    boost::container::flat_multiset<mark_ptr, comp> m_marks;
};

inline bool mark_set::comp::
operator()(mark_ptr const& lhs, mark_ptr const& rhs) const
{
    return lhs->location() < rhs->location();
}

inline bool mark_set::comp::
operator()(mark_ptr const& lhs, mark const& rhs) const
{
    return lhs->location() < rhs.location();
}

inline bool mark_set::comp::
operator()(mark const& lhs, mark_ptr const& rhs) const
{
    return lhs.location() < rhs->location();
}

inline bool mark_set::comp::
operator()(mark_ptr const& lhs, location_t const& rhs) const
{
    return lhs->location() < rhs;
}

inline bool mark_set::comp::
operator()(location_t const& lhs, mark_ptr const& rhs) const
{
    return lhs < rhs->location();
}

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_MARK_SET_HPP
