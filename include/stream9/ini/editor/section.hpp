#ifndef STREAM9_INI_EDITOR_SECTION_HPP
#define STREAM9_INI_EDITOR_SECTION_HPP

#include "element.hpp"
#include "entry.hpp"
#include "type.hpp"

#include <iosfwd>
#include <optional>
#include <string_view>
#include <vector>

namespace stream9::ini::editors {

class editor;
class buffer;
class mark;
class region;

class section
{
public:
    section(section const&) = delete;
    section& operator=(section const&) = delete;

    section(section&&) = default;
    section& operator=(section&&) = default;

    ~section() = default;

    // accessor
    std::string_view name() const;

    // query
    location_t top() const { return m_top->location(); }
    location_t head() const;
    location_t bottom() const;

    value* find(std::string_view key);
    value const* find(std::string_view key) const;

    std::string_view find(std::string_view key, std::string_view fallback) const;

    value&       find_or_throw(std::string_view key);
    value const& find_or_throw(std::string_view key) const;

    bool contains(std::string_view key) const;
    count_t size() const { return m_entries.size(); }
    bool empty() const { return m_entries.empty(); }

    std::string_view text() const;

    // modifier
    void append(std::string_view key, std::string_view value);
    value& find_or_append(std::string_view key);

    bool erase(std::string_view key);

    // command
    void rename(std::string_view new_name);
    void clear();

    // iterator
    auto begin() const { return m_entries.begin(); }
    auto end() const { return m_entries.end(); }

    // operator
    value const& operator[](std::string_view key) const { return find_or_throw(key); }
    value&       operator[](std::string_view key) { return find_or_append(key); }

private:
    friend class editor;
    friend class parser;

    section(editor&);
    section(editor&, class region const& name, location_t top);
    section(editor&, location_t top);

    static std::unique_ptr<section> make(editor&);

    static std::unique_ptr<section>
        make(editor&, class region const& name, location_t top);

    class buffer& buffer();
    class buffer const& buffer() const;

    void append(class region const& key, class region const& value);
    void move_to(location_t);
    void increment_line() { ++m_num_lines; }
    void append_empty_line();

private:
    using entries = std::vector<entry>;

    editor* m_editor; // non-null
    std::optional<section_name> m_name;
    unique_mark_ptr m_top; // non-null
    length_t m_num_lines = 0;
    entries m_entries;
};

std::ostream& operator<<(std::ostream&, section const&);

struct section_line
{
    std::string line;
    location_t name_loc;
    length_t name_len;
};

section_line make_section_line(std::string_view name);

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_SECTION_HPP
