#ifndef STREAM9_INI_EDITORS_ELEMENT_HPP
#define STREAM9_INI_EDITORS_ELEMENT_HPP

#include "mark.hpp"
#include "region.hpp"
#include "type.hpp"

#include <iosfwd>
#include <string>
#include <string_view>

namespace stream9::ini::editors {

class buffer;

class element
{
public:
    element(class buffer&, class region const&, std::string value);
    virtual ~element() = default;

    element(element const&) = delete;
    element& operator=(element const&) = delete;

    element(element&&) = default;
    element& operator=(element&&) = default;

    // accessor
    class buffer& buffer() const { return *m_buffer; }
    std::string const& text() const { return m_value; }

    // query
    location_t location() const { return *m_mark; }
    length_t length() const { return m_length; }

    // modifier
    void assign(std::string_view);
    void shift_mark(offset_t);

    // operator
    element& operator=(std::string_view const s) { assign(s); return *this; }
    operator std::string const& () const { return text(); }
    operator std::string_view () const { return text(); }
    bool operator==(std::string_view const s) const { return s == m_value; }
    bool operator!=(std::string_view const s) const { return s != m_value; }

private:
    virtual std::string escape(std::string_view) const = 0;

private:
    class buffer* m_buffer; // non-null
    unique_mark_ptr m_mark; // non-null
    length_t m_length;
    std::string m_value;
};

std::ostream& operator<<(std::ostream&, element const&);

class section_name : public element
{
public:
    section_name(class buffer&, class region const&);

    using element::operator=;

private:
    // override element
    std::string escape(std::string_view) const override;
};


class key : public element
{
public:
    key(class buffer&, class region const&);

    using element::operator=;

private:
    // override element
    std::string escape(std::string_view) const override;
};

class value : public element
{
public:
    value(class buffer&, class region const&);

    using element::operator=;

private:
    // override element
    std::string escape(std::string_view) const override;
};

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITORS_ELEMENT_HPP

