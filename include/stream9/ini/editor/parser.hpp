#ifndef STREAM9_INI_EDITOR_PARSER_HPP
#define STREAM9_INI_EDITOR_PARSER_HPP

#include "type.hpp"

#include "../scanner.hpp"
#include "../error.hpp"

#include <string_view>
#include <vector>

namespace stream9::ini::editors {

class editor;
class section;

class parser : public event_handler
{
public:
    parser(editor&, std::vector<parse_error>* errors = nullptr);

    void run();

    // override event_handler
    void on_section(std::string_view name) override;

    void on_entry(std::string_view key,
                  std::string_view value) override;

    void on_newline(std::string_view content) override;

    void on_error(error_code code, std::string_view range) override;

private:
    location_t get_location(std::string_view) const;
    void handle_error(error_code code, location_t) const;
    std::string_view current_line() const;

private:
    editor& m_editor;
    std::string_view m_text;
    section* m_current = nullptr;
    int m_line_no = 1;
    std::string_view::const_iterator m_bol;
    std::vector<parse_error>* m_errors = nullptr;
    bool m_skip_section = false;
};

} // namespace stream9::ini::editors

#endif // STREAM9_INI_EDITOR_PARSER_HPP
