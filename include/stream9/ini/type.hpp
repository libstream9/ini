#ifndef STREAM9_INI_TYPE_HPP
#define STREAM9_INI_TYPE_HPP

#include <string>

namespace stream9::ini {

using name_t = std::string;
using value_t = std::string;

} // namespace stream9::ini

#endif // STREAM9_INI_TYPE_HPP
