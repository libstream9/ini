#ifndef STREAM9_INI_ERROR_HPP
#define STREAM9_INI_ERROR_HPP

#include <filesystem>
#include <stdexcept>
#include <string>
#include <string_view>

namespace stream9::ini {

enum class error_code
{
    invalid_char,
    new_line_is_expected,
    key_value_separator_is_expected,
    section_name_is_expected,
    section_terminator_is_expected,
    empty_key,
    duplicate_key,
    duplicate_section_name,
    invalid_section_name,
    unknown_escape_sequence,
    unclosed_quote,
    no_such_section,
    no_such_key,
    cant_move_section,
};

std::string to_string(error_code);

std::ostream& operator<<(std::ostream&, error_code const&);

class error : public std::runtime_error
{
public:
    error(error_code code);
    error(error_code code, char const* what);

    // accessor
    auto code() const { return m_code; }

private:
    error_code m_code;
};

class invalid_data : public error
{
public:
    invalid_data(error_code code, std::string_view range)
        : error { code }
        , m_range { range }
    {}

    // accessor
    auto range() const { return m_range; }

private:
    std::string_view m_range;
};

class scan_error : public invalid_data
{
public:
    scan_error(error_code code, std::string_view range)
        : invalid_data { code, range }
    {}
};

class parse_error : public error
{
public:
    using path_t = std::filesystem::path;

public:
    parse_error(error_code code, int line_no, int position,
                std::string_view line, path_t);

    // accessor
    path_t const& path() const { return m_path; }
    int line_no() const { return m_line_no; }
    int position() const { return m_position; }

private:
    path_t m_path;
    int m_line_no;
    int m_position;
};

} // namespace stream9::ini

#endif // STREAM9_INI_ERROR_HPP
