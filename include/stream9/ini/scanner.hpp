#ifndef STREAM9_INI_SCANNER_HPP
#define STREAM9_INI_SCANNER_HPP
//
// Grammar
//
// ini_file := *(blank_line | comment_line | section | entry)
//
// section := *space '[' *space section_name *space ']' *space ?comment new_line
//
// section_name := quoted_name | unquoted_section_name
// unquoted_section_name := (unquoted_section_char - quote_mark) *(unquoted_section_char)
// unquoted_section_char := escape_seq | (char - (']' | new_line | comment_mark))
//
// entry := *space key *space '=' *space value *space ?comment new_line
//
// key := quoted_name | unquoted_key
// unquoted_key := (unquoted_key_char - quote_mark) *(unquoted_key_char)
// unquoted_key_char := escape_seq | (char - ('=' | comment_mark | new_line))
//
// value := quoted_value | unquoted_value
// quoted_value := quote_mark +(quoted_value_char - quote_mark) quote_mark
// quoted_value_char := continuation | escape_seq | (char - new_line)
// unquoted_value := (unquoted_value_char - quote_mark) *unquoted_value_char
// unquoted_value_char := continuation | escape_seq | (char - (comment_mark | new_line))
//
// comment_line := *space comment new_line
// comment := comment_mark *(continuation | char)
// comment_mark := '#' | ';'
//
// quoted_name := quote_mark +(quoted_name_char - quote_mark) quote_mark
// quoted_name_char := escape_seq | (char - new_line)
//
// escape_seq := "\\" | "\'" | '\"' | "\0" | "\a" | "\b" | "\t" | "\r"
//               | "\n" | "\;" | "\#" | "\=" | "\[" | "\]"
//               | "\x<4 hexdigit UTF-16 seq>
//
// blank_line := new_line
//
// continuation := '\' new_line
//
// new_line := '\n' | '\r' | "\r\n"
//
// quote_mark := "'" | '"'
//
// space := ' ' | '\t'
//
// char := <any character> - ctrl_char
//
// ctrl_char := <0x00-0x1F, 0x7F> - '\t'
//
#include "error.hpp"

#include <string_view>

namespace stream9::ini {

class event_handler
{
public:
    virtual ~event_handler() = default;

    virtual void on_section(std::string_view /*name*/) {}

    virtual void on_entry(std::string_view /*key*/, std::string_view /*value*/) {}

    virtual void on_comment(std::string_view /*content*/) {}

    virtual void on_newline(std::string_view /*content*/) {}

    virtual void on_error(error_code const code, std::string_view const content)
    {
        throw scan_error(code, content);
    }
};

void scan(std::string_view /*content*/, event_handler&);

} // namespace stream9::ini

#endif // STREAM9_INI_SCANNER_HPP
