#ifndef STREAM9_INI_ESCAPE_HPP
#define STREAM9_INI_ESCAPE_HPP

#include "error.hpp"

#include <string>
#include <string_view>

namespace stream9::ini {

std::string escape_section(std::string_view);
std::string escape_key(std::string_view);
std::string escape_value(std::string_view);

std::string unescape(std::string_view);

std::string unescape_continuation(std::string_view);

std::string_view unquote(std::string_view);

class unknown_escape_sequence : public invalid_data
{
public:
    unknown_escape_sequence(std::string_view range)
        : invalid_data { error_code::unknown_escape_sequence, range } {}
};

} // namespace stream9::ini

#endif // STREAM9_INI_ESCAPE_HPP
